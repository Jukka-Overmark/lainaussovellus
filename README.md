***THIS IS A COPY OF A GROUP PROJECT. WORK IN PROGRESS***

### Riippuvuudet

### JUnit Platform

- **Group ID**: `org.junit.platform`
- **Version**: `1.3.1`

### JUnit Jupiter 

- **Group ID**: `org.junit.jupiter`
- **Version**: `5.3.1`

### Hibernate Core

- **Group ID**: `org.hibernate`
- **Version**: `5.3.6.Final`

### Hibernate Annotations

- **Group ID**: `org.hibernate.common`
- **Version**: `5.0.4.Final`

### MySQL Connector

- **Group ID**: `mysql`
- **Version**: `5.1.36`

### JSON Simple

- **Group ID**: `com.googlecode.json-simple`
- **Version**: `1.1.1`