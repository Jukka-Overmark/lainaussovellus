package sovellus;

import java.io.IOException;

import Kontrollerit.MainKontrolleri;
import hibernate.Kayttaja;
import hibernate.TietokantaAccessObject;
import hibernate.Tietokanta_IF;
import hibernate.Tuote;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application{

	private Stage primaryStage;
	private AnchorPane layout;
	private static Tietokanta_IF tietokantaDAO;
	private Kayttaja kayttaja; // = new Kayttaja("testailu", false, false, "oulu");
	private Tuote[] tuotteet;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Lainaussovellus");

		tietokantaDAO = new TietokantaAccessObject();
		// tuotteet = tietokantaDAO.tulostaTuotteet();

		initRootLayout();

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	public void initRootLayout() {
		try {


			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/FXML/LoginOrRegister.fxml"));

			AnchorPane layout = (AnchorPane) loader.load();

			// Give the controller access to the main app.

			MainKontrolleri controller = (MainKontrolleri) loader.getController();
			controller.setMainApp(this);

			Scene scene = new Scene(layout);
			primaryStage.setScene(scene);
		//	primaryStage.getIcons().add(new Image("/kuvat/LIC_läpinäkyvä.png"));
			primaryStage.show();


		} catch (IOException e) {
			e.printStackTrace();
		}
		
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent t) {
		    	Platform.exit();
		        System.exit(0);
		    }
		});
	}
	

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public Tietokanta_IF getDAO() {
		return tietokantaDAO;
	}

	public void setKayttaja (Kayttaja k) {
		this.kayttaja = k;
	}

	public Kayttaja getKayttaja() {
		return kayttaja;
	}
	
	public Tuote[] getTuotteet() {
		return tuotteet;
	}
	
	public void setTuotteet (Tuote[] tuotteet) {
		this.tuotteet = tuotteet;
	}


	/*
    public void naytaTuotelista() {

        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/FXML/Tuotteet.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Tuotelista");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);


            // TuotelistaKontrolleri controller = loader.getController();

            // Show the dialog and wait until the user closes it
            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	 */


}

