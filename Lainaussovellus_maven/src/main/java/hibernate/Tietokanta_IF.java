package hibernate;

/**
 * @author Toni Hämäläinen 28.2.2019
 */
public interface Tietokanta_IF {
	
	public abstract boolean luoKayttaja(Kayttaja k);
	public abstract boolean listaaTuote(Tuote t);
	public abstract boolean listaaTuote(Kayttaja k, Tuote t);
	public abstract boolean lainaaTuote(Kayttaja k, int id);
	public abstract Tuote[] tulostaTuotteet();
	public abstract Tuote tulostaTuote(int id);
	public abstract boolean poistaTuote(int id);
	public abstract Kayttaja haeKayttaja(int id);
}
