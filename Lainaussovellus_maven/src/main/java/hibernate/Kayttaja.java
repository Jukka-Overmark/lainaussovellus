package hibernate;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
* @author Toni Hämäläinen 31.1.2019
*/

@Entity
@Table(name="kayttaja")
public class Kayttaja {
	
	@Column(name="nimi", nullable=false)
	private String nimi;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="kayttajaId", nullable=false)
	private int kayttajaId;
	
	@Type(type="org.hibernate.type.NumericBooleanType")
	@Column (name="onkoEstetty", nullable=false)
	private boolean onkoEstetty;
	
	@Type(type="org.hibernate.type.NumericBooleanType")
	@Column (name="onkoYllapitaja", nullable=false)
	private boolean onkoYllapitaja;
	
	@Column (name="xkoordinaatti", nullable=false)
	private double xkoord;
	@Column (name="ykoordinaatti", nullable=false)
	private double ykoord;
	
	// @Type(type="org.hibernate.type.NumericBooleanType") 	// onkoLainannut : boolean, tuote
	// @Column (name="onkoLainannut", nullable=false)		// ei vielä hajuu miten toteuttaa tässä
	// private boolean onkoLainannut;
	
	@Column(name="sijainti", nullable=false)
	private String sijainti;
	
	@OneToMany(mappedBy="tuotteenLainaaja", orphanRemoval=true)
	private Set<Tuote> lainatutTuotteet = new HashSet<Tuote>();
	
	@OneToMany(mappedBy="tuotteenOmistaja", orphanRemoval=true)
	private Set<Tuote> omistetutTuotteet = new HashSet<Tuote>();

	public Kayttaja() {
	
		
	}
	
	public Kayttaja(String nimi, int kayttajaId, boolean onkoEstetty, boolean onkoYllapitaja, String sijainti) {
		super();
		this.nimi = nimi;
		this.kayttajaId = kayttajaId;
		this.onkoEstetty = onkoEstetty;
		this.onkoYllapitaja = onkoYllapitaja;
		this.sijainti = sijainti;
	}
	
	public Kayttaja(String nimi, boolean onkoEstetty, boolean onkoYllapitaja, String sijainti) {
		super();
		this.nimi = nimi;
		this.onkoEstetty = onkoEstetty;
		this.onkoYllapitaja = onkoYllapitaja;
		this.sijainti = sijainti;
	}
	
	public Kayttaja(String nimi, boolean onkoEstetty, boolean onkoYllapitaja, String sijainti, double xkoord, double ykoord) {
		super();
		this.nimi = nimi;
		this.onkoEstetty = onkoEstetty;
		this.onkoYllapitaja = onkoYllapitaja;
		this.sijainti = sijainti;
		this.xkoord = xkoord;
		this.ykoord = ykoord;
	}
	
	/**
	 * Returns the name of a user
	 * 
	 * @return the name
	 */
	public String getNimi() {
		return nimi;
	}

	/**
	 * Sets the name for the user
	 * 
	 * @param nimi the name to set
	 */
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	/**
	 * Returns the user's id
	 * 
	 * @return the user id
	 */
	public int getKayttajaId() {
		return kayttajaId;
	}

	/**
	 * Sets the user's id
	 * 
	 * @param kayttajaId the user id to set
	 */
	public void setKayttajaId(int kayttajaId) {
		this.kayttajaId = kayttajaId;
	}

	/**
	 * Returns boolean true or false if the user is banned
	 * 
	 * @return true or false
	 */
	public boolean isOnkoEstetty() {
		return onkoEstetty;
	}

	/**
	 * Sets the user's ban status to true or false
	 * 
	 * @param onkoEstetty boolean to set the ban status
	 */
	public void setOnkoEstetty(boolean onkoEstetty) {
		this.onkoEstetty = onkoEstetty;
	}

	/**
	 * Returns boolean true or false if the user is an admin
	 * 
	 * @return true or false
	 */
	public boolean isOnkoYllapitaja() {
		return onkoYllapitaja;
	}

	/**
	 * Sets the user's admin status to true or false
	 * 
	 * @param onkoYllapitaja boolean to set the admin status
	 */
	public void setOnkoYllapitaja(boolean onkoYllapitaja) {
		this.onkoYllapitaja = onkoYllapitaja;
	}

	/**
	 * Returns the user's location in String format
	 * 
	 * @return user's location
	 */
	public String getSijainti() {
		return sijainti;
	}

	/**
	 * Sets the user's location
	 * 
	 * @param sijainti the location to set
	 */
	public void setSijainti(String sijainti) {
		this.sijainti = sijainti;
	}

	public Set<Tuote> getLainatutTuotteet() {
		return lainatutTuotteet;
	}
	

	public Set<Tuote> getOmistetutTuotteet() {
		return omistetutTuotteet;
	}

	/**
	 * Returns the user's latitude coordinate
	 * 
	 * @return the latitude coordinate
	 */
	public double getXkoord() {
		return xkoord;
	}

	/**
	 * Sets the user's latitude coordinate
	 * 
	 * @param xkoord the latitude coordinate to set
	 */
	public void setXkoord(double xkoord) {
		this.xkoord = xkoord;
	}

	/**
	 * Returns the user's longitude coordinate
	 * 
	 * @return the longitude coordinate
	 */
	public double getYkoord() {
		return ykoord;
	}

	/**
	 * Sets the user's longitude coordinate
	 * 
	 * @param ykoord the longitude coordinate to set
	 */
	public void setYkoord(double ykoord) {
		this.ykoord = ykoord;
	}
	
	

}
