package hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;


/**
* @author Toni Hämäläinen 31.1.2019
*/

public class TietokantaAccessObject implements Tietokanta_IF {
	
	private static SessionFactory istuntotehdas;
	private final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
	
	public TietokantaAccessObject() {
		try {
			istuntotehdas = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			System.out.println("Oh no");
			StandardServiceRegistryBuilder.destroy(registry);
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean luoKayttaja(Kayttaja k) {
		Session istunto = istuntotehdas.openSession();
		Transaction transaktio = null;
		boolean onnistuiko = false;
		
		try {
			transaktio = istunto.beginTransaction();
			istunto.saveOrUpdate(k);
			transaktio.commit();
			onnistuiko = true;
		} catch (Exception e) {
			if (transaktio != null) {
				transaktio.rollback();
				e.printStackTrace();
				onnistuiko = false;
			}
		} finally {
			istunto.close();
		}
		
		return onnistuiko;
	}
	
	@Override
	public boolean listaaTuote(Tuote tuote) { // tuotteen lisäys ilman "kirjautumista"
		Session istunto = istuntotehdas.openSession();
		Transaction transaktio = null;
		boolean onnistuiko = false;
		
		try {
			transaktio = istunto.beginTransaction();
			istunto.saveOrUpdate(tuote);
			transaktio.commit();
			onnistuiko = true;
		} catch (Exception e) {
			if (transaktio != null) {
				transaktio.rollback();
				e.printStackTrace();
			}
		} finally {
			istunto.close();
		}
		
		return onnistuiko;
	}	
	
	@Override
	public boolean listaaTuote(Kayttaja kayttaja, Tuote tuote) { // tuotteen lisäys "kirjautuneena" sisään
		Session istunto = istuntotehdas.openSession();
		Transaction transaktio = null;
		boolean onnistuiko = false;
		String omistaja = kayttaja.getNimi();
		String sijainti = kayttaja.getSijainti();
		String tuoteNimi = tuote.getNimi();
		String tuoteTyyppi = tuote.getTyyppi();
		
		try {
			transaktio = istunto.beginTransaction();
			// istunto.saveOrUpdate(new Tuote(tuoteNimi, omistaja, tuoteTyyppi, sijainti));
			istunto.save(new Tuote(kayttaja, tuoteNimi, tuoteTyyppi));
			transaktio.commit();
			onnistuiko = true;
		} catch (Exception e) {
			if (transaktio != null) {
				transaktio.rollback();		
				e.printStackTrace();
			}
		} finally {
			istunto.close();
		}
		
		return onnistuiko;
	}
	
	@Override
	public boolean lainaaTuote(Kayttaja kayttaja, int id) { 
		Session istunto = istuntotehdas.openSession();
		Transaction transaktio = null;
		boolean onnistuiko = false;
		String omistaja = kayttaja.getNimi();
		String sijainti = kayttaja.getSijainti();
		
		try {
			transaktio = istunto.beginTransaction();
			Tuote t = (Tuote)istunto.get(Tuote.class, id);
			//Tuote t = tulostaTuote(id);
			// Hibernate.initialize(kayttaja);
			// Hibernate.initialize(t);
			
			if (t != null) {
				t.asetaLainaaja(kayttaja);
			} else {
				System.out.println("Tuotetta ei löytynyt");
			}
			transaktio.commit();
			onnistuiko = true;
		} catch (Exception e) {
			if (transaktio != null) {
				transaktio.rollback();		
				e.printStackTrace();
			}
		} finally {
			
			istunto.close();
		}
		
		return onnistuiko;
	}
	
	@Override
	public Tuote[] tulostaTuotteet() {
		ArrayList<Tuote> tuotteet = new ArrayList<Tuote>();
		Session istunto = istuntotehdas.openSession();
		
		try { 
			istunto.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Tuote> result = istunto.createQuery("from Tuote").getResultList(); // haetaan kaikki tuotteet tuote-taulusta
			
			for (Tuote t : result) { // käydään tuote-lista läpi ja lisätään ne arraylistiin
				tuotteet.add(t);
			}
			
			istunto.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("error");
			e.printStackTrace();
		} finally {
			istunto.close();
		}
		Tuote[] returnArray = new Tuote[tuotteet.size()];
		return (Tuote[]) tuotteet.toArray(returnArray); // casti tuote-olioksi ja palautetaan Tuote-listinä
	}
	
	@Override
	public Tuote tulostaTuote(int id) {
		Session istunto = istuntotehdas.openSession();
		Tuote t = new Tuote();
		/*
		Query q = istunto.createQuery("from Tuote t where t.id=:tuoteId").setString("tuoteId", String.valueOf(id));
		Tuote t = (Tuote) q.uniqueResult();  //new Tuote();
		istunto.close();
		return t;
		*/

		
		try {
			istunto.beginTransaction();
			istunto.load(t, id);
			istunto.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			istunto.close();
		}
		return t;
		
	}
	
	@Override
	public boolean poistaTuote(int id) {
		Session istunto = istuntotehdas.openSession();
		boolean onnistuiko = false;
		Tuote t;
		
		try {
			istunto.beginTransaction();
			t = (Tuote) istunto.get(Tuote.class, id);
			if (t != null) {
				istunto.delete(t);
				onnistuiko = true;
		}
			istunto.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			onnistuiko = false;
		} finally {
			istunto.close();
		}	
		return onnistuiko;
	}

	@Override
	public Kayttaja haeKayttaja(int id) throws ObjectNotFoundException{
		
		Session istunto = istuntotehdas.openSession();
		Kayttaja k = new Kayttaja();

		try {
			istunto.beginTransaction();
			k = istunto.get(Kayttaja.class, id);
			istunto.getTransaction().commit();
		} catch (ObjectNotFoundException oe) {
			istunto.close();
			return null;
		} catch (Exception e) { 
			e.printStackTrace();
		} finally {
			istunto.close();
		}
		return k;
		
	}
	
	
	
}
