package hibernate;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Type;


/**
* @author Toni Hämäläinen 31.1.2019
*/

@Entity
@Table(name="tuote")
public class Tuote {
	@Column(name="nimi", nullable=false)
	private String nimi;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tuoteId", nullable=false)
	private int tuoteId;
	
	@Column(name="omistaja", nullable=false)
	private String omistaja;
	
	// @Column(name="lainaaja")
	// private String lainaaja;
	
	@Column(name="tyyppi", nullable=false)
	private String tyyppi;
	
	@Column(name="sijainti", nullable=false)
	private String sijainti;
	
	private double etaisyys;
	
	@Type(type="org.hibernate.type.NumericBooleanType")
	@Column (name="onkoLainassa", nullable=false)
	private boolean onkoLainassa;
	
	@Column(name="palautuspvm")
	@Temporal(TemporalType.DATE) // https://thoughts-on-java.org/hibernate-tips-map-java-util-date-database-column/
	private Date palautuspvm;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="lainaajaId")
	private Kayttaja tuotteenLainaaja;
	
	@ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	@JoinColumn(name="omistajaId", updatable=false)
	private Kayttaja tuotteenOmistaja;

	public Tuote() {
		
	}
	
	public Tuote(String nimi, int tuoteId, String omistaja, String tyyppi, String sijainti) {
		super();
		this.nimi = nimi;
		this.tuoteId = tuoteId;
		this.omistaja = omistaja;
		this.tyyppi = tyyppi;
		this.sijainti = sijainti;
	}
	
	public Tuote(String nimi, String omistaja, String tyyppi, String sijainti) {
		super();
		this.nimi = nimi;
		this.omistaja = omistaja;
		this.tyyppi = tyyppi;
		this.sijainti = sijainti;
	}
	

	public Tuote(String nimi, String tyyppi) {
		super();
		this.nimi = nimi;
		this.tyyppi = tyyppi;
	}
	
	public Tuote (Kayttaja omistaja, String nimi, String tyyppi) {
		super();
		this.tuotteenOmistaja = omistaja;
		this.nimi = nimi;
		this.tyyppi = tyyppi;
		this.omistaja = omistaja.getNimi();
		this.sijainti = omistaja.getSijainti();
	}

	/**
	 * @return the nimi
	 */
	public String getNimi() {
		return nimi;
	}

	/**
	 * @param nimi the nimi to set
	 */
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	/**
	 * @return the tuoteId
	 */
	public int getTuoteId() {
		return tuoteId;
	}

	/**
	 * @param tuoteId the tuoteId to set
	 */
	public void setTuoteId(int tuoteId) {
		this.tuoteId = tuoteId;
	}

	/**
	 * @return the omistaja
	 */
	public String getOmistaja() {
		return omistaja;
	}

	/**
	 * @param omistaja the omistaja to set
	 */
	public void setOmistaja(String omistaja) {
		this.omistaja = omistaja;
	}


	/**
	 * @return the tyyppi
	 */
	public String getTyyppi() {
		return tyyppi;
	}

	/**
	 * @param tyyppi the tyyppi to set
	 */
	public void setTyyppi(String tyyppi) {
		this.tyyppi = tyyppi;
	}

	/**
	 * @return the sijainti
	 */
	public String getSijainti() {
		return sijainti;
	}

	/**
	 * @param sijainti the sijainti to set
	 */
	public void setSijainti(String sijainti) {
		this.sijainti = sijainti;
	}
	
	public double getEtaisyys() {
		return etaisyys;
	}
	
	public void setEtaisyys(double etaisyys) {
		this.etaisyys = etaisyys;
	}

	/**
	 * @return the palautuspvm
	 */
	public Date getPalautuspvm() {
		return palautuspvm;
	}

	/**
	 * @param palautuspvm the palautuspvm to set
	 */
	public void setPalautuspvm(Date palautuspvm) {
		this.palautuspvm = palautuspvm;
	}

	/**
	 * @return the onkoLainassa
	 */
	public boolean isOnkoLainassa() {
		return onkoLainassa;
	}

	/**
	 * @param onkoLainassa the onkoLainassa to set
	 */
	public void setOnkoLainassa(boolean onkoLainassa) {
		this.onkoLainassa = onkoLainassa;
	}

	public Kayttaja getLainaaja() {
		return tuotteenLainaaja;
	}
	
	public void asetaLainaaja(Kayttaja kayttaja) {
		this.tuotteenLainaaja = kayttaja;
		kayttaja.getLainatutTuotteet().add(this);
	}
	
	public void asetaOmistaja(Kayttaja kayttaja) {
		this.tuotteenOmistaja = kayttaja;
		kayttaja.getOmistetutTuotteet().add(this);
	}	
	
	public Kayttaja getTuotteenOmistaja() {
		return tuotteenOmistaja;
	}
	
	@Override
	public String toString() {
		return this.tuoteId + ", " + this.nimi + ", " + this.omistaja + ", " + this.sijainti + ", " + this.tyyppi;
	}
	
//	/**
//	 * @return the lainaaja
//	 */
//	public String getLainaaja() {
//		return lainaaja;
//	}
//
//	/**
//	 * @param lainaaja the lainaaja to set
//	 */
//	public void setLainaaja(String lainaaja) {
//		this.lainaaja = lainaaja;
//	}
	
}
