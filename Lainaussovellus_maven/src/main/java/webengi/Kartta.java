package webengi;

import java.io.File;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author JukeL
 */
public class Kartta {
    
    Scene scene;
    WebEngine engine;
    WebView wv;
    
    public Kartta(){
        wv = new WebView();

        engine = wv.getEngine();
        File f = new File("");
        //System.out.println(f.getAbsoluteFile());
        
        engine.load(f.toURI()+"src\\main\\java\\webengi\\index.html");
    }

    /**
    * JavaFX:ssä käytettävä näkymä.
    * <p>
    * @return      WebView
    */
    public WebView getView() {
        return wv;
    }
    /**
    * Palauttaa haetun osoitteen Stringinä muodossa: 'Leveysaste Pituusaste Katu Numero'.
    * <p>
    * Jos osoitetta ei löytynyt palauttaa nullin.
    * @param  haku  String
    * @return      String
    */
    public String haeOsoite(String haku){
        try {
                String coords;
                coords = OpenStreetMapUtils.getInstance().getCoordinates(haku);
                String[] palat = coords.split(" ");
                if(palat.length < 4){
                    return null;
                }
                return coords;
            } catch (Exception ex) {
                return "Error: "+ex;
            }
    }
    /**
    * Lähetä koodi Javascript-muodossa
    * <p>
    * Esim: "function tervehdys(){ console.log("Hello world"); }"
    * @param  data  String
    */
    public void lahetaSkripti(String data){
        engine.executeScript(data);
    }
    /**
    * Aseta karttaan merkki.
    * <p>
    * @param  lat  Leveysaste
    * @param  lon  Pituusaste
    * @param  String  Popup-teksti
    * @param  nayta true: Näytä popup aina. false: Näytä popup painettaessa.
    */
    public void asetaMerkki(double lat, double lon, String popup, boolean nayta){
    	String naytateksti = "";
    	if(nayta)
    		naytateksti = ".openPopup()";
        engine.executeScript("var marker = L.marker(["+lat+","+ lon+"]).addTo(mymap).bindPopup(\""+popup+"\")"+naytateksti+";");
    }
    /**
    * Kohdista karttanäkymä koordinaattiin.
    * <p>
    * @param  lat  Leveysaste
    * @param  lon  Pituusaste
    */
    public void asetaNakyma(String lat, String lon){
        int zoom = (int) engine.executeScript("mymap.getZoom();");
        engine.executeScript("mymap.setView(["+lat+", "+lon+"],"+zoom+");");
    }
    
    public double degreesToRadians(double degrees) {
    	return degrees * Math.PI / 180;
	}

	public double distanceInKmBetweenEarthCoordinates(double lat1, double lon1, double lat2, double lon2) {
		int earthRadiusKm = 6371;
		
		double dLat = degreesToRadians(lat2-lat1);
		double dLon = degreesToRadians(lon2-lon1);
		
		lat1 = degreesToRadians(lat1);
		lat2 = degreesToRadians(lat2);
		
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		
		return (double)Math.round(earthRadiusKm * c *10)/10;
	}
}
