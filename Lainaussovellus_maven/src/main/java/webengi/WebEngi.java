package webengi;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author JukeL
 */
public class WebEngi extends Application {
    
    @Override
    public void start(Stage primaryStage) throws InterruptedException {
        HBox hbox = new HBox();
        TextField teksti = new TextField();
        Kartta kartta = new Kartta();
        
        /*kartta.getView().getEngine().getLoadWorker().stateProperty().addListener((obs, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
                // new page has loaded, process:
                kartta.asetaMerkki("60.220952", "24.804817", "hellou");
            }
        });*/
        
        hbox.getChildren().addAll(kartta.getView(), teksti);
        /*teksti.setOnAction((e) -> {
           
                String osoite = kartta.haeOsoite(teksti.getText());
                if(osoite.equals("Osoitetta ei löytynyt"))
                    return;
                String[] palat = osoite.split(" ");
                
                kartta.asetaMerkki(palat[0], palat[1], "hellou");
                kartta.asetaNakyma(palat[0], palat[1]);
            
        });*/
        Scene s = new Scene(hbox);
        primaryStage.setScene(s);
        //System.out.println(kartta.haeOsoite("vanha maantie 6"));
        primaryStage.show();

        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        launch(args);
    }
    
}
