package Kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import hibernate.Kayttaja;
import hibernate.Tuote;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sovellus.Main;
import webengi.Kartta;

public class MainKontrolleri implements Initializable{

	@FXML
	private Button selaa;

	//    @FXML
	//    private Button tuoteLainattavaksi;

	//    @FXML
	//    private Button lainaaTuote;

	@FXML
	private Button loginPage;
	@FXML
	private Button registerPage;
	@FXML
	private Button back;
	@FXML
	private Button takaisin;
	@FXML
	private TextArea tuoteLista;
	@FXML
	private Button naytabtn;
	@FXML
	private HBox karttaView;
	@FXML
	private TextField firstNameField;
	@FXML
	private TextField cityField;
	@FXML
	private Button lisaaTuoteButton;
	@FXML
	private TextField tuoteNimiField;
	@FXML
	private TextField tuoteTyyppiField;
	@FXML
	private TextField loginUserNameField;
	@FXML
	private Button tuoteTietokantaanButton;
	@FXML
	private Button login;
	@FXML
	private Text kirjautumisNimi;
	@FXML
	private Button naytakartta;
	@FXML
	private ImageView image;
	@FXML
    private TextField hakuKentta;
	@FXML
	private Button logout;
	@FXML
	private TableView tuoteTableView;
	@FXML
	private Button tableViewBtn;
	@FXML
	private TableColumn tableIdColumn;
	@FXML
	private TableColumn tableNameColumn;
	@FXML
	private TableColumn tableOmistajaColumn;
	@FXML
	private TableColumn tableTyyppiColumn;
	@FXML
	private TableColumn tableSijaintiColumn;
	@FXML
	private TableColumn tableEtaisyysColumn;
    @FXML
    private Button home;
    @FXML
    private Button lainausBtn;

    @FXML
    private HBox karttaTuoteView;
    @FXML
    private Label labelLat;
    @FXML
    private Label labelLon;

	private static Main mainApp;
	private static Kartta kartta;

	public MainKontrolleri() {


		kartta = new Kartta();
		// System.out.println(mainApp + " cons");

	}

	//    @FXML
	//    public void Tuotteet(MouseEvent event) throws IOException {
	//		selaa.getScene().getWindow().hide();
	//		
	//		Stage tuotteet = new Stage();
	//		Parent root = FXMLLoader.load(getClass().getResource("/FXML/Tuotteet.fxml"));
	//		Scene scene = new Scene(root);
	//		tuotteet.setScene(scene);
	//		tuotteet.show();
	//		tuotteet.setResizable(false);
	//    }


	public void initialize(URL arg0, ResourceBundle arg1) {

		// System.out.println(mainApp + " init");

		if (mainApp != null) {
			
			if (mainApp.getKayttaja() != null) {
				System.out.println(mainApp.getKayttaja().getNimi());
				
				if (kirjautumisNimi != null) {
					kirjautumisNimi.setText(mainApp.getKayttaja().getNimi());
				}
			}	
		}
	}

	@FXML
	private void selaa (ActionEvent e) throws IOException {


		// selaa.getScene().getWindow().hide();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/Tuotteet.fxml"));
		Parent parent = loader.load();
		((Stage)selaa.getScene().getWindow()).setScene(new Scene(parent));
		
		if(mainApp.getTuotteet() == null)
			mainApp.setTuotteet(mainApp.getDAO().tulostaTuotteet());
		
		/*
		Stage tuotteet = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/Tuotteet.fxml"));
		Scene scene = new Scene(root);
		tuotteet.setScene(scene);
		tuotteet.show();
		tuotteet.setResizable(false);
		 */
	}

	public void setMainApp(Main mainApp) {
		this.mainApp = mainApp;
	}


	@FXML
	private void loginPage (ActionEvent e) throws IOException {

		//loginPage.getScene().getWindow().hide();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/Login.fxml"));
		Parent parent = loader.load();
		((Stage)loginPage.getScene().getWindow()).setScene(new Scene(parent));

		/*
		Stage login = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/Login.fxml"));
		Scene scene = new Scene(root);
		login.setScene(scene);
		login.show();
		login.setResizable(false);
		 */
	}

	@FXML
	private void registerPage (ActionEvent e) throws IOException {

		//registerPage.getScene().getWindow().hide();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/Register.fxml"));
		Parent parent = loader.load();
		((Stage)registerPage.getScene().getWindow()).setScene(new Scene(parent));

		/*
		Stage register = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/Register.fxml"));
		Scene scene = new Scene(root);
		register.setScene(scene);
		register.show();
		register.setResizable(false);
		 */
	}

	@FXML
	private void karttaPage(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/Kartalla.fxml"));
		Parent parent = loader.load();
		((Stage)naytakartta.getScene().getWindow()).setScene(new Scene(parent));	  
	}

	@FXML
	void lataaKartalle(ActionEvent event) {
		karttaView.getChildren().add(kartta.getView());
		Tuote[] lista = mainApp.getDAO().tulostaTuotteet();

		


	}
	

	@FXML
    void listaaHaku(KeyEvent event) {
		System.out.println(hakuKentta.getText());
    }
	
	@FXML
	private void back (ActionEvent e) throws IOException {

		//back.getScene().getWindow().hide();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/LoginOrRegister.fxml"));
		Parent parent = loader.load();
		((Stage)back.getScene().getWindow()).setScene(new Scene(parent));

		/*
		Stage takasin = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/MainPage.fxml"));
		Scene scene = new Scene(root);
		takasin.setScene(scene);
		takasin.show();
		takasin.setResizable(false);
		 */
	}
	
	@FXML
	private void takaisin (ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/MainPage.fxml"));
		Parent parent = loader.load();
		((Stage)back.getScene().getWindow()).setScene(new Scene(parent));
	}
	
	@FXML
	private void home (ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/MainPage.fxml"));
		Parent parent = loader.load();
		((Stage)home.getScene().getWindow()).setScene(new Scene(parent));
	}

	@FXML
	private void getTableViewItems (ActionEvent e) throws IOException {
		System.out.println("test");
		
		ArrayList<Tuote> tuotelisti = new ArrayList<Tuote>();
		
		if (mainApp.getTuotteet() == null) {
			mainApp.setTuotteet(mainApp.getDAO().tulostaTuotteet());
		}
				
		for (Tuote t : mainApp.getTuotteet()) {
			tuotelisti.add(t);
			t.setEtaisyys(kartta.distanceInKmBetweenEarthCoordinates(t.getTuotteenOmistaja().getXkoord(), t.getTuotteenOmistaja().getYkoord(), mainApp.getKayttaja().getXkoord(), mainApp.getKayttaja().getYkoord()));
		}
		ObservableList<Tuote> obsList = FXCollections.observableArrayList(tuotelisti);
		tableNameColumn.setCellValueFactory(new PropertyValueFactory<Tuote, String>("nimi"));
		tableIdColumn.setCellValueFactory(new PropertyValueFactory<Tuote, Integer>("tuoteId"));
		tableOmistajaColumn.setCellValueFactory(new PropertyValueFactory<Tuote, String>("omistaja"));
		tableTyyppiColumn.setCellValueFactory(new PropertyValueFactory<Tuote, String>("tyyppi"));
		tableSijaintiColumn.setCellValueFactory(new PropertyValueFactory<Tuote, String>("sijainti"));
		tableEtaisyysColumn.setCellValueFactory(new PropertyValueFactory<Tuote, Double>("etaisyys"));
		tuoteTableView.setItems(obsList);
		
		//Kartalle
		karttaTuoteView.getChildren().add(kartta.getView());

		for (Tuote t : tuotelisti) {
			kartta.asetaMerkki(t.getTuotteenOmistaja().getXkoord(), t.getTuotteenOmistaja().getYkoord(), t.getNimi(), true);
		}
		
	}
	
    @FXML
    private void tuoteValittu(MouseEvent e) {
    	System.out.println(((Tuote)tuoteTableView.getSelectionModel().getSelectedItem()).getTuoteId());
    }
    
	@FXML
	private void lainaaTuote() {
		if (tuoteTableView.getSelectionModel().getSelectedItem() == null) {
			System.out.println("ei mitää valittu");
		} else {
			Tuote lainattavaTuote = (Tuote) tuoteTableView.getSelectionModel().getSelectedItem();
			mainApp.getDAO().lainaaTuote(mainApp.getKayttaja(), lainattavaTuote.getTuoteId());
			System.out.println("lainattu");
		}
	}
	
	@FXML
    void addressTyped(KeyEvent event) {
		
		if(event.getCode() == KeyCode.BACK_SPACE)
			return;
		
		if(karttaTuoteView.getChildren().size() == 0) {
			karttaTuoteView.getChildren().add(kartta.getView());
			kartta.asetaMerkki(0, 0, "", false);
		}
			
		String pala = kartta.haeOsoite(cityField.getText());
		if(pala == null)
			return;
		String[] palat = pala.split(" ");
		kartta.lahetaSkripti("marker.setLatLng(["+palat[0]+","+palat[1]+"])");
		labelLat.setText(palat[0]);
		labelLon.setText(palat[1]);
		kartta.asetaNakyma(palat[0],palat[1]);
    }
	
	@FXML
	private void naytaTuotteet (ActionEvent e) throws IOException {

		/*
		// System.out.println(mainApp);
		
		mainApp.setTuotteet(mainApp.getDAO().tulostaTuotteet());
		
		tuoteLista.setText("");
		for (Tuote t : mainApp.getTuotteet()) {
			// System.out.println(t + "\n");
			tuoteLista.appendText(t.toString() + "\n");	
		}	*/
	}

	@FXML
	private void registerAccount (ActionEvent e) throws IOException {

		String nimi = firstNameField.getText();
		String kaupunki = cityField.getText();
		double xkoord = Double.parseDouble(labelLat.getText());
		double ykoord = Double.parseDouble(labelLon.getText());

		if (!nimi.isEmpty() && !kaupunki.isEmpty()) {
			mainApp.setKayttaja(new Kayttaja(nimi, false, false, kaupunki, xkoord, ykoord)); //= new Kayttaja(nimi, false, false, kaupunki);

			mainApp.getDAO().luoKayttaja(mainApp.getKayttaja());

			takaisin(e);
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Virhe");
			alert.setHeaderText("Käyttäjän luonnissa virhe");
			alert.setContentText("Tarkista, että kentät eivät ole tyhjät");
			alert.showAndWait();
		}
	}
	
	@FXML
	private void logoutAction (ActionEvent e) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/LoginOrRegister.fxml"));
		Parent parent = loader.load();
		((Stage)logout.getScene().getWindow()).setScene(new Scene(parent));
		
		mainApp.setKayttaja(null);
	}

	@FXML
	private void lisaaTuote (ActionEvent e) throws IOException {

		//lisaaTuoteButton.getScene().getWindow().hide();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/FXML/LisaaTuote.fxml"));
		Parent parent = loader.load();
		((Stage)lisaaTuoteButton.getScene().getWindow()).setScene(new Scene(parent));

		/*
		Stage lisaaTuoteStage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/LisaaTuote.fxml"));
		Scene scene = new Scene(root);
		lisaaTuoteStage.setScene(scene);
		lisaaTuoteStage.show();
		lisaaTuoteStage.setResizable(false);
		 */
	}

	@FXML
	private void lisaaTuoteTietokantaan() {

		String nimi = tuoteNimiField.getText();
		String tyyppi = tuoteTyyppiField.getText();

		Tuote t = new Tuote(nimi, tyyppi);

		if (mainApp.getKayttaja() == null) {

			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Virhe");
			alert.setHeaderText("Tuotteen lisäyksessä virhe");
			alert.setContentText("Varmista, että olet kirjautuneena sisään, jotta voit lisätä tuotteen");
			alert.showAndWait();
		}
		if (nimi.isEmpty() || tyyppi.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Virhe");
			alert.setHeaderText("Tuotteen lisäyksessä virhe");
			alert.setContentText("Tarkista, että kentät eivät ole tyhjät");
			alert.showAndWait();
		} else {
			mainApp.getDAO().listaaTuote(mainApp.getKayttaja(), t);
		}	

	}

	@FXML
	private void userLogin(ActionEvent e) throws IOException {

		String kayttaja = loginUserNameField.getText();
		Kayttaja k = null;

		try {
			int kId = Integer.parseInt(kayttaja);
			//System.out.println("testi");


			k = mainApp.getDAO().haeKayttaja(kId);
			//	System.out.println(k + " kaytt");

			if (k == null) {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setTitle("Virhe");
				alert.setHeaderText("Kirjautumisessa virhe");
				alert.setContentText("Käyttäjää ei löytynyt");
				alert.showAndWait();
			} else {
				mainApp.setKayttaja(k);
				takaisin(e);
				//	System.out.println(k.getNimi() + " asetettu");
			}
		} catch (NumberFormatException nEx) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Virhe");
			alert.setHeaderText("Kirjautumisessa virhe");
			alert.setContentText("Varmista, että annoit oikean id:n (integer)");
			alert.showAndWait();
		}
	}

}
