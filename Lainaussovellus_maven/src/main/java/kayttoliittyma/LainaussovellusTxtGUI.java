package kayttoliittyma;

import java.util.Scanner;

import hibernate.Kayttaja;
import hibernate.TietokantaAccessObject;
import hibernate.Tuote;

/**
 * @author Toni Hämäläinen 31.1.2019
 */

public class LainaussovellusTxtGUI {

	static TietokantaAccessObject tietokantaDAO = new TietokantaAccessObject();
	static Scanner lukija = new Scanner(System.in);
	private static Kayttaja kayttaja;

	public static void listaaTuote() {
		String nimi;
	// 	int tuoteId;
		String omistaja;
		String tyyppi;
		String sijainti;

		System.out.println("Anna tuotteen nimi: ");
		nimi = lukija.nextLine();
	// 	System.out.println("Anna tuotteen id: ");
	// 	tuoteId = Integer.parseInt(lukija.nextLine());
		System.out.println("Anna tuotteen omistaja: ");
		omistaja = lukija.nextLine();
		System.out.println("Anna tuotteen tyyppi: ");
		tyyppi = lukija.nextLine();
		System.out.println("Anna sijainti");
		sijainti = lukija.nextLine();

		if (tietokantaDAO.listaaTuote(new Tuote(nimi, omistaja, tyyppi, sijainti))) {
			System.out.println("Tuotteen lisäys onnistui");
		} else {
			System.out.println("Tuotteen lisäys epäonnistui");
		}
	}
	
	public static void listaaTuote(Kayttaja k) {
		String nimi;
		String tyyppi;

		System.out.println("Anna tuotteen nimi: ");
		nimi = lukija.nextLine();
		System.out.println("Anna tuotteen tyyppi: ");
		tyyppi = lukija.nextLine();

		if (tietokantaDAO.listaaTuote(k, new Tuote(nimi, tyyppi))) {
			System.out.println("Tuotteen lisäys onnistui");
		} else {
			System.out.println("Tuotteen lisäys epäonnistui");
		}
	}
	
	public static void lainaaTuote(Kayttaja k) {
		int id;
		
		System.out.println("Anna lainattavan tuotteen id");
		id = Integer.parseInt(lukija.nextLine());
		
		if (tietokantaDAO.lainaaTuote(k, id)) {
			System.out.println("Onnistui");
		} else {
			System.out.println("Epäonnistui");
		}
	}
	
	public static void tulostaTuotteet() {
		
		System.out.println("Nimi -- Omistaja -- Tyyppi -- Sijainti");
		
		for (Tuote t : tietokantaDAO.tulostaTuotteet()) {
			System.out.println(t);
		}
		
	}
	
	public static void poistaTuote() {
		int id;
		
		System.out.println("Anna poistettavan tuotteen id");
		id = Integer.parseInt(lukija.nextLine());
		
		if (tietokantaDAO.poistaTuote(id)) {
			System.out.println("Poisto onnistui");
		} else {
			System.out.println("Poisto epäonnistui");
		}
	}

	public static void main(String[] args) {
		
		kayttaja = new Kayttaja("pete", false, true, "Helsinki");
		tietokantaDAO.luoKayttaja(kayttaja);

		char valinta;
		final char CREATE = 'L', READ = 'R', UPDATE = 'U', DELETE = 'D', QUIT = 'Q', LAINAA = 'K';

		do {

			System.out.println("\nL: Lisää uusi tuote tietokantaan\n"
					+ "R: Listaa tietokannassa olevien tuotteiden tiedot\n"
					+ "U: Päivitä tuotteen tiedot tietokantaan\n"
					+ "D: Poista tuote tietokannasta\n"
					+ "K: Lainaa tuote\n"
					+ "Q: Lopetus");
			System.out.println("Valintasi: ");
			valinta = (lukija.nextLine().toUpperCase()).charAt(0);
			switch (valinta) {
			case READ:
				tulostaTuotteet();
				break;
			case CREATE:
				if (kayttaja == null) {
					listaaTuote();
				} else {
					listaaTuote(kayttaja);
				}
				break;
			case LAINAA:
				if (kayttaja != null) {
					lainaaTuote(kayttaja);
				}
				break;
			case UPDATE:
				// TODO
				break;
			case DELETE:
				poistaTuote();
				break;
			}
		} while (valinta != QUIT);

		System.out.println("Lopetettiin.");
		System.exit(0);

	}

}

