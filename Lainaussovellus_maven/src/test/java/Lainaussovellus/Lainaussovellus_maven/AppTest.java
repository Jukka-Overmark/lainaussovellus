package Lainaussovellus.Lainaussovellus_maven;


import static org.junit.jupiter.api.Assertions.*;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import hibernate.Kayttaja;
import hibernate.TietokantaAccessObject;
import hibernate.Tuote;

/**
* @author Toni Hämäläinen 7.2.2019
*/

public class AppTest {
	
	 private static SessionFactory istuntotehdas;
	 private final static StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
	 private static TietokantaAccessObject accObj;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		
		try {
			accObj = new TietokantaAccessObject();
		} catch (Exception e) {
			System.out.println("Oh no");
		StandardServiceRegistryBuilder.destroy(registry);
			e.printStackTrace();
		}
		
		
		try {
			istuntotehdas = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			System.out.println("Oh no");
			StandardServiceRegistryBuilder.destroy(registry);
			e.printStackTrace();
		}
		
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}
	
	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	@Disabled
	void testTietokantaAccessObject() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testLuoTuote() {
		
		String nimi = "polkupyörä";
		String omistaja = "Matti";
		String tyyppi = "pyörä";
		String sijainti = "Helsinki";
		
		Tuote t = new Tuote(nimi, omistaja, tyyppi, sijainti);
		
		assertEquals("pyörä", t.getTyyppi());
	}
	
	@Test
	public void testLuoKayttaja() {
		Kayttaja k = new Kayttaja("testi", false, false, "Testipaikka");

		assertEquals(true, accObj.luoKayttaja(k));
	}
	
	@Test
	public void testListaaTuoteTuote() {
		String nimi= "testi";
		String tyyppi = "testituote";
		
		Kayttaja k = new Kayttaja("Toni", false, true, "Helsinki");
		accObj.luoKayttaja(k);
		
		Tuote t = new Tuote(nimi, tyyppi);
		
		assertEquals(true, accObj.listaaTuote(k, t));	
		
	}

	@Test
	@Disabled
	void testListaaTuoteKayttajaTuote() {
		fail("Not yet implemented");
	}

	@Test
	@Disabled
	void testLainaaTuote() {
		fail("Not yet implemented");
	}

	@Test
	@Disabled
	void testTulostaTuotteet() {
		fail("Not yet implemented");
	}

	@Test
	void testTulostaTuote() {
		
		String nimi= "tulostustuote";
		String tyyppi = "testituote";
		
		Kayttaja k = new Kayttaja("testailija", false, false, "testimesta");
		accObj.luoKayttaja(k);
		Tuote t = new Tuote(nimi, tyyppi);
		accObj.listaaTuote(k, t);
		
		assertEquals("tulostustuote", accObj.tulostaTuote(3).getNimi()); // tää vähän huono ku ei voi tietää millä id:llä se menee tietokantaan
	}

	@Test
	void testPoistaTuote() {
		
		String nimi= "poistotuote";
		String tyyppi = "testituote";
		
		Kayttaja k = new Kayttaja("testailija", false, false, "testimesta");
		accObj.luoKayttaja(k);
		Tuote t = new Tuote(nimi, tyyppi);
		accObj.listaaTuote(k, t);
		
		assertEquals(true, accObj.poistaTuote(1)); // sama juttu ku testTulostaTuotteessa
	}

}

