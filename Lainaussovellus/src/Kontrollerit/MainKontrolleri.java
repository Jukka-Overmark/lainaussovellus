package Kontrollerit;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
//import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MainKontrolleri implements Initializable{

    @FXML
    private Button selaa;

    @FXML
    private Button tuoteLainattavaksi;

    @FXML
    private Button lainaaTuote;
    
//    @FXML
//    public void Tuotteet(MouseEvent event) throws IOException {
//		selaa.getScene().getWindow().hide();
//		
//		Stage tuotteet = new Stage();
//		Parent root = FXMLLoader.load(getClass().getResource("/FXML/Tuotteet.fxml"));
//		Scene scene = new Scene(root);
//		tuotteet.setScene(scene);
//		tuotteet.show();
//		tuotteet.setResizable(false);
//    }
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
	
	@FXML
	public void selaa (ActionEvent e) throws IOException {
		
		selaa.getScene().getWindow().hide();
		
		Stage tuotteet = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/Tuotteet.fxml"));
		Scene scene = new Scene(root);
		tuotteet.setScene(scene);
		tuotteet.show();
		tuotteet.setResizable(false);
	}

}
